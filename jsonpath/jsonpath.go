package jsonpath

import "errors"
import "strconv"

type Decoder struct {
	path       string
	readOffset int
}

type Selector int8

const (
	Property Selector = iota
	Index
)

func NewDecoder(s string) *Decoder {
	if s[0] == '$' {
		s = s[1:]
	}
	return &Decoder{
		path: s,
	}
}

func (d *Decoder) Next() (s Selector, val string, err error) {
	if d.path[d.readOffset] == '[' {
		d.readOffset++
		quote := ""
		escaped := false
		start := d.readOffset
		for ; d.readOffset < len(d.path); d.readOffset++ {
			if escaped {
				escaped = false
				continue
			}
			switch d.path[d.readOffset] {
			case ']':
				if s == Property {
					v, err := strconv.Unquote(d.path[start:d.readOffset])
					if err != nil {
						return s, d.path[start:d.readOffset], err
					}
					return s, v, nil
				} else if s == Index {
					v := d.path[start:d.readOffset]
					_, err := strconv.Atoi(v)
					if err != nil {
						return s, v, err
					}
					return s, v, nil
				}
			case '\\':
				escaped = true
			case '\'':
				if quote == "" {
					quote = "'"
				} else if quote == "'" {
					quote = ""
				}
			case '"':
				if quote == "" {
					quote = "\""
				} else if quote == "\"" {
					quote = ""
				}
			}
		}
		return s, d.path[start:d.readOffset], nil

	} else if d.path[d.readOffset] == '.' {
		d.readOffset++
		s = Property
		start := d.readOffset
		for ; d.readOffset < len(d.path); d.readOffset++ {
			switch d.path[d.readOffset] {
			case '[':
				fallthrough
			case '.':
				return s, d.path[start:d.readOffset], nil
			}
		}
		return s, d.path[start:d.readOffset], nil
	}
	return s, "", errors.New("Missing Seperator")
}

func (d *Decoder) More() bool {
	if d.readOffset < len(d.path) {
		return true
	}
	return false
}
