package jschema

import (
	"encoding/json"
	"io"

	"errors"

	"strconv"

	"bitbucket.org/miltonryan/jschema/jsonpath"
)

//Schema TODO
type Schema struct {
	rules map[string]Rule
}

//Set defaines the rules for the given path.
//If a rule is already set for the given path, it is overridden.
//path must be a valid json selector.
//paths start with $ to represent the json object
func (s *Schema) Set(path string, r ...Rule) {
	if s.rules == nil {
		s.rules = make(map[string]Rule)
	}

	s.rules[path] = All(r...)
}

//Enforce enforces rules
func (s *Schema) Enforce(r io.Reader) error {
	var v interface{}

	dc := json.NewDecoder(r)
	if err := dc.Decode(&v); err != nil {
		return err
	}

	for key, rule := range s.rules {
		if err := assertRule(key, rule, v); err != nil {
			return err
		}
	}

	return nil
}

func assertRule(key string, rule Rule, v interface{}) error {
	d := jsonpath.NewDecoder(key)
	for d.More() {
		s, k, err := d.Next()
		if err != nil {
			return err
		}

		if s == jsonpath.Property {
			if val, ok := v.(map[string]interface{}); ok {
				v, ok = val[k]
				if !ok {
					rule.Test(key, nil)
				}
				continue
			} else {
				return errors.New("jsonpath issue")
			}
		} else if s == jsonpath.Index {
			i, err := strconv.Atoi(k)
			if err != nil {
				return errors.New("jsonpath issue")
			}
			if val, ok := v.([]interface{}); ok {
				if i < len(val) {
					v = val[i]
				} else {
					rule.Test(key, nil)
				}
				continue
			} else {
				return errors.New("jsonpath issue")
			}
		}
	}

	if err := rule.Test(key, v); err != nil {
		return err
	}

	return nil
}
