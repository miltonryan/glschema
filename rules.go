package jschema

import (
	"errors"
	"strconv"
)

type Rule interface {
	Test(string, interface{}) (err error)
}

//RuleFunc Convenience Method
type RuleFunc func(string, interface{}) error

//Test Implements Rule interface
func (r RuleFunc) Test(s string, v interface{}) error {
	return r(s, v)
}

var UnsupportedTypeErr error = errors.New("unsuported type")

//Useful default rules

//All means all rules
func All(rules ...Rule) Rule {
	if len(rules) == 1 {
		return rules[0]
	}
	return RuleFunc(func(n string, v interface{}) error {
		for i := range rules {
			if err := rules[i].Test(n, v); err != nil {
				return err
			}
		}
		return nil
	})
}

//
//Type Rules
//

//String asserts that v is a string
func String() Rule {
	return RuleFunc(func(n string, v interface{}) error {
		_, ok := v.(string)
		if !ok {
			return errors.New(n + " must be a string")
		}

		return nil
	})
}

func Boolean() Rule {
	return RuleFunc(func(n string, v interface{}) error {
		_, ok := v.(bool)
		if !ok {
			return errors.New(n + " must be a boolean")
		}

		return nil
	})
}

func Required() Rule {
	return Null()
}

func Optional() Rule {
	return RuleFunc(func(n string, v interface{}) error {
		return nil
	})
}

func Null() Rule {
	return RuleFunc(func(n string, v interface{}) error {
		if v == nil {
			return errors.New(n + " is required")
		}
		return nil
	})
}

func Number() Rule {
	return Float()
}

func Float() Rule {
	return RuleFunc(func(n string, v interface{}) error {
		_, ok := v.(float64)
		if !ok {
			return errors.New(n + " must be a number")
		}

		return nil
	})
}

func Integer() Rule {
	return RuleFunc(func(n string, v interface{}) error {
		i, ok := v.(float64)
		if !ok || int(i*10)%10 != 0 {
			return errors.New(n + " must be an Integer")
		}

		return nil
	})
}

//
//Properties
//

func Length(r Rule) Rule {
	return RuleFunc(func(n string, v interface{}) error {
		n = "len(" + n + ")"
		switch v := v.(type) {
		case string:
			return r.Test(n, float64(len(v)))
		case []interface{}:
			return r.Test(n, float64(len(v)))
		default:
			return UnsupportedTypeErr
		}

	})

}

//
//Comparisons
//

//GT Greater than
func GT(i float64) Rule {
	return RuleFunc(func(n string, v interface{}) error {
		switch v := v.(type) {
		case float64:
			if v <= i {
				return errors.New("(" + ftos(v) + " > " + ftos(i) + ")")
			}
		default:
			return UnsupportedTypeErr
		}
		return nil
	})
}

func LT(i float64) Rule {
	return RuleFunc(func(n string, v interface{}) error {
		switch v := v.(type) {
		case float64:
			if v >= i {
				return errors.New("(" + ftos(v) + " < " + ftos(i) + ")")
			}
		default:
			return UnsupportedTypeErr
		}

		return nil
	})
}

func EQ(i interface{}) Rule {
	return RuleFunc(func(n string, v interface{}) error {
		//TODO:equal
		if v != v {

		}
		return nil
	})
}

func ftos(f float64) string {
	return strconv.FormatFloat(f, 'f', -1, 64)
}
