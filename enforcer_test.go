package jschema_test

import "testing"
import "bitbucket.org/miltonryan/jschema"
import "github.com/franela/goblin"
import "strings"

func TestSchema(t *testing.T) {
	g := goblin.Goblin(t)

	var s jschema.Schema

	g.Describe("Schema", func() {
		g.BeforeEach(func() {
			s = jschema.Schema{}
		})

		g.It("should enforce on value", func() {
			s.Set("$", jschema.GT(-1))
			err := s.Enforce(strings.NewReader("-1"))
			g.Assert(err != nil).IsTrue()
		})
		g.It("should enforce on object", func() {
			s.Set("$.exists", jschema.Required())
			err := s.Enforce(strings.NewReader("{\"exists\":{}}"))
			g.Assert(err).Equal(nil)
		})
		g.It("should enforce on array", func() {
			s.Set("$", jschema.Length(jschema.LT(5)))
			err := s.Enforce(strings.NewReader("[6,5,4,3,2,1]"))
			g.Assert(err != nil).IsTrue()
		})

	})
}
